<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class JobsModel extends Model
{
	/**
	 * [FunctionName description]
	 * @param string $value [description]
	 */
	public function uploadFile($file)
	{
		$result = true;

		foreach ($file as $key => $value) {
			$insert = DB::table('jobs')->insert($value);
			if($insert) :
				$result = true;
		else :
			$result = false;
			break;
			endif;
		}

		return $result;
	}

   /**
    * [getAllJobs description]
    * @return [type] [description]
    */
   public function getAllJobs()
   {
      $result = (DB::table('jobs')
                  ->select(
                     'id',
                     'job_title',
                     'location',
                     'date'
                  )
                  ->get())->toArray();

      return $result;
   }   

   /**
    * [getJobDetails description]
    * @param  [type] $id [description]
    * @return [type]     [description]
    */
   public function getJobDetails($id)
   {
      $result = (DB::table('jobs')
                     ->where('id',$id)
                     ->select(
                        'id',
                        'job_title',
                        'job_description',
                        'date',
                        'location',
                        'applicants'
                     )
                     ->get()
                  )->toArray();

      return $result;
   }
}
