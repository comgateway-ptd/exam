<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends InitiatorController
{
    /**
	 * [index description]
	 * @return [type] [description]
	 */
    public function index()
    {
    	$page_title = 'Dashboard';

    	return view('pages.dashboard.dashboard', compact('page_title'));
    }

    /**
     * [uploadFile description]
     * @return [type] [description]
     */
    public function uploadFile(Request $request)
    {
       	$file = $request->file('file');

       	$attached_file  =   $this->csv_to_array($file, ',');

       	$import = $this->jobsModel->uploadFile($attached_file);

       	if($import == true) :
       		return redirect()->back()->with('success', 'Import Success!');
   		else :
   			return redirect()->back()->with('danger', 'Import Failed! Please check the file you are trying to import.');
		endif;
    }

    /**
     * [csv_to_array description]
     * @param  [type] $filename  [description]
     * @param  string $delimiter [description]
     * @return [type]            [description]
     */
    public function csv_to_array($filename, $delimiter=',')
    {
    	if(!file_exists($filename) || !is_readable($filename))
	        return FALSE;

	    $header = NULL;
	    $data = array();
        $null_row_header = [];
        $row_headers    = [];

	    if (($handle = fopen($filename, 'r')) !== FALSE):
	        while(($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE):
                if(!$header):
                    foreach ($row as $row_key => $row_value):
                        if($row_value == NULL):
                            array_push($null_row_header, $row_key);
                        else:
                            array_push($row_headers, $row_value);
                        endif;
                    endforeach;
                    
                    $header = $row_headers;
	            else:
                    if($null_row_header != NULL):
                        $new_row_values =   [];

                        foreach ($row as $row_key1 => $row_value1):
                            $row_checker    =   NULL;
                            $row_checker    =   array_search($row_key1,$null_row_header);
                            $row_validation =   ($row_checker === 0 || $row_checker != NULL ? 1 : NULL);

                            if($row_validation == NULL):
                                $new_row_values[$row_key1]  =   $row_value1;
                            endif;
                        endforeach;
                    else:
                        $new_row_values =   $row;
                    endif;

                    if(array_filter($new_row_values)):
                        $data[] = array_combine($header, $new_row_values);
                    endif;
                endif;
	        endwhile;
	        fclose($handle);
	    endif;
        
	    return $data;
    }
}
