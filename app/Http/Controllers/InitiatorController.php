<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\JobsModel;

class InitiatorController extends Controller
{
    protected $jobsModel;

	/**
	 * [__construct description]
	 */
	public function __construct()
	{
		$this->jobsModel = new JobsModel();
	}

	/**
	 * [object_to_array description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function object_to_array($data)
    {
        if (is_array($data) || is_object($data))
        {
            $result = array();
            foreach ($data as $key => $value)
            {
                $result[$key] = $this->object_to_array($value);
            }
            return $result;
        }
        else
        {
            return $data;
        }
    }
}
