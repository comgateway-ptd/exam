<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JobsController extends InitiatorController
{
	/**
	 * [index description]
	 * @return [type] [description]
	 */
    public function index()
    {
    	$page_title = 'Jobs';
    	$jobs = $this->jobsModel->getAllJobs();

    	return view('pages.jobs.jobs', compact('page_title','jobs'));
    }

    /**
     * [viewJobDetails description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function viewJobDetails($id)
    {
    	$page_title = 'Jobs';
    	$job_detail = $this->jobsModel->getJobDetails($id);

    	if(!empty($job_detail)) :
    		$job_detail = $job_detail[0];
		else :
			$job_detail = 'empty';
		endif;

    	return view('pages.jobs.job-detail', compact('page_title','job_detail'));
    }
}
