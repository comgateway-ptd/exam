<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Dashboard
Route::get('dashboard', 'DashboardController@index');
Route::post('upload', 'DashboardController@uploadFile');

// Jobs
Route::get('jobs', 'JobsController@index');
Route::get('view-job/{id}', 'JobsController@viewJobDetails');