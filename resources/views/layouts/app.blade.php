<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $page_title }}</title>

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/logo') }}/logomark.min.svg">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/logo') }}/logomark.min.svg">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="{{ asset('js/components/font-awesome/font-awesome.min.js') }}"></script>
    <script src="{{ asset('js/components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/components/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/components/selectpicker/bootstrap-select.min.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/components/sb-admin/sb-admin-2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/components/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/components/selectpicker/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('page-css')
</head>
<body id="page-top">
    <div id="wrapper">
        @include('includes.sidebar')

        <main id="content-wrapper">
            @yield('content')

            @yield('page-js')
        </main>
    </div>
    
    <script src="{{ asset('js/components/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/components/sb-admin/sb-admin-2.min.js') }}"></script>
</body>
</html>
