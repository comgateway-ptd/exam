@extends('layouts.app')

@section('page-css')

@endsection

@section('content')

	<div class="container" id="content">
		<div class="row">
			<div class="col-md-12 mb-4">
				<div class="card">
					<div class="card-header bg-primary text-white"><i class="fas fa-user-tie"></i> Jobs</div>
					<div class="card-body">
						<table class="table table-bordered table-hover table-striped" id="jobs-table">
							<thead>
								<th>Job Title</th>
								<th>Job Description</th>
								<th>Location</th>
								<th>Date</th>
								<th>Applicants</th>
							</thead>
							@if(empty($job_detail))
								<tbody></tbody>
							@else
								<tbody>
									<tr class="font-weight-bold">
										<td>{{ $job_detail->job_title }}</td>
										<td>{{ $job_detail->job_description }}</td>
										<td>{{ $job_detail->date }}</td>
										<td>{{ $job_detail->location }}</td>
										<td>{{ $job_detail->applicants }}</td>
									</tr>
								</tbody>
							@endif
						</table>
						<div class="col-md-12 text-right pr-0 mt-5">
							<a href="{{ url('jobs') }}">
								<button class="btn btn-sm btn-danger">Back</button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('page-js')
	<script type="text/javascript">
		jobsPageFunctions.onLoad();
    </script>
@endsection