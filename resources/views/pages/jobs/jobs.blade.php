@extends('layouts.app')

@section('page-css')

@endsection

@section('content')

	<div class="container" id="content">
		<div class="row">
			<div class="col-md-12 mb-4">
				<div class="card">
					<div class="card-header bg-primary text-white"><i class="fas fa-user-tie"></i> Jobs</div>
					<div class="card-body">
						<table class="table table-bordered table-hover table-striped" id="jobs-table">
							<thead>
								<th>Job Title</th>
								<th>Location</th>
								<th>Date</th>
								<th class="text-center">Action</th>
							</thead>
							@if(empty($jobs))
								<tbody></tbody>
							@else
								<tbody>
									@foreach($jobs as $index => $value)
										<tr>
											<td>{{ $value->job_title }}</td>
											<td>{{ $value->location }}</td>
											<td>{{ $value->date }}</td>
											<td class="text-center">
												<a href="{{ url('view-job') }}/{{ $value->id }}">
													<button class="btn btn-sm btn-primary"><i class="fas fa-folder-open"></i> View Details</button>
												</a>
											</td>
										</tr>
									@endforeach
								</tbody>
							@endif
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('page-js')
	<script type="text/javascript">
		jobsPageFunctions.onLoad();
    </script>
@endsection