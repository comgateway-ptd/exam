@extends('layouts.app')

@section('page-css')

@endsection

@section('content')

	<div class="container" id="content">
		<div class="row">
			<div class="col-md-7 offset-md-2 mb-4">
				<div class="card">
					<div class="card-header bg-primary text-white"><i class="fas fa-cloud-upload-alt"></i> Upload</div>
					<div class="card-body">
						<form action="{{ url('upload') }}" method="POST" enctype="multipart/form-data">
							@csrf
							<div class="form-inline">
								<span class="text-danger mb-3"><b>Note: All column headers should NOT have spaces. Instead of spaces use underscore (_) to separate words.</b></span>
								<div class="form-group">
									<input type="file" name="file">
								</div>
								<button class="btn btn-sm btn-primary" id="js-upload-submit">Upload files</button>
							</div>
						</form>

						@if(session()->has('success'))
	                        <div class="alert alert-success mt-3">
	                            <b>{{ session()->get('success') }}</b>
	                        </div>
	                    @endif
	                    @if(session()->has('danger'))
	                        <div class="alert alert-danger mt-3">
	                            <b>{{ session()->get('danger') }}</b>
	                        </div>
	                    @endif

					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('page-js')
	<script type="text/javascript">
        $('#dashboard-link').addClass('active');
    </script>
@endsection