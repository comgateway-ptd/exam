/**
 * [jobsPageFunctions description]
 * @type {Object}
 */
jobsPageFunctions = {
	/**
	 * [onLoad description]
	 * @return {[type]} [description]
	 */
	onLoad: function() 
	{
		$('#jobs-link').addClass('active');
		jobsPageFunctions.initializeDataTables();
	},

	/**
	 * [initializeDataTables description]
	 * @return {[type]} [description]
	 */
	initializeDataTables: function()
	{
		$('.table').DataTable();
	}
}