/**
 * [adminsPageFunctions description]
 * @type {Object}
 */
dashboardPageFunctions = {

	/**
	 * [onLoad description]
	 * @return {[type]} [description]
	 */
	onLoad: function()
	{
		$('#admins-link').addClass('active');
        $('#admins-table').DataTable();
	},

	initializeGeneralSelectpicker: function()
	{
		$('.selectpicker').selectpicker();
	}
}